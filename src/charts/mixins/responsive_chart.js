import echarts from 'echarts';
import { debounce } from 'underscore';

export default {
  data() {
    return {
      onResize: debounce(this.setChartSize, 30),
    };
  },
  created() {
    window.addEventListener('resize', this.onResize);
  },
  beforeDestory() {
    window.removeEventListener('resize', this.onResize);
  },
  mounted() {
    this.initChart();
  },
  methods: {
    initChart() {
      const div = document.createElement('div');
      this.$refs.chart.appendChild(div);
      this.chart = echarts.init(div, null, { renderer: this.renderer });
      this.draw();
    },
    draw() {
      this.chart.setOption(this.options);
      this.setChartSize();
    },
    setChartSize() {
      const [rect] = this.$refs.chartContainer.getClientRects();
      this.chart.resize({
        width: rect.width,
        height: rect.height ? rect.height : this.minHeight || 400,
      });
    },
  },
  watch: {
    renderer() {
      this.chart.dispose();
      this.initChart();
    },
  },
};
