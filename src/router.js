import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'demo',
      component: () => import('./views/demo.vue'),
    },
    {
      path: '/notes',
      name: 'notes',
      component: () => import('./views/notes.vue'),
    },
  ],
});
